# Musala Soft Drone Application Assignment

The drone web service allows users to manage drones. The service exposes REST APIs to add, update, fetch, load and filter drones. The drone REST API accepts and provides JSON for all endpoints.


## Architecture

The drone web service was implemented based on the Model-View-Controller(MVC) architectural pattern. MVC separates an application into three main logical components: model, view and controller were each component is built to handle a specific aspect of an application depending on purpose. This segregation makes the application modifiable, scalable and easy to maintain.

## Frameworks and Modules Used

|**Feature**         |      **Framework/Module**|
|--------------------|--------------------------| 
|Drone application	 |      Springboot          |
|Logging             |      SLF4J               |
|Database            |     H2                   |

## Prerequisites
- JDK 11
- SpringBoot 2.7.6
- Maven 3
- Postman

## Assumptions

The following assumptions were made during the development of the solution:
- Each drone carries 1 'medication' at a time
- Battery level is measured in whole numbers ranging from 0-100
- All drones will be charging at idle at a rate of 10% every hour
- Loading, loaded, delivered states consume 1% every hour
- Delivering consumes 3% battery every hour
- Returning consumes 2% battery every hour

## Steps to build and run drone web service

The default profile creates an embedded in-memory database. The drone, medication and drone_battery_log tables will be created automatically when the application is run. The database only has three tables.

create database dronedb;
create user sa with password ‘password’;
grant all privileges on database dronedb to sa;

1.	Open terminal
2.	Navigate to the project folder
3.	Run “mvn clean install”
4.	Run “java -jar target/ drone-0.0.1-SNAPSHOT.jar”

You can test the application with the provided Postman collection:
https://api.postman.com/collections/25019223-3a9f0a5b-1425-44ce-9589-01ea1c4f435e?access_key=PMAT-01GP36CT4WW3JCQCYKZM3J4FMK

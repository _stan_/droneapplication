INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000120X', 'IDLE', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000121X', 'IDLE', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000123X', 'DELIVERING', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000124X', 'DELIVERED', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000122X', 'RETURNING', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000125X', 'LOADED', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000126X', 'LOADING', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000127X', 'DELIVERING', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000129X', 'DELIVERED', '500');
INSERT INTO DRONE (battery_capacity, model, serial_number, state, weight_limit) values (50, 'HEAVYWEIGHT', 'HW700000128X', 'RETURNING', '500');

INSERT INTO MEDICATION (code, name, weight) VALUES ( 'GLU12', 'Glucostix', 400);
INSERT INTO MEDICATION (code, name, weight) VALUES ( 'COL', 'Colcaps', 250);
INSERT INTO MEDICATION (code, name, weight) VALUES ( 'USA', 'Cascade', 750);
INSERT INTO MEDICATION (code, name, weight) VALUES ( 'SYR','Honey Syrup',500);
INSERT INTO MEDICATION (code, name, weight) VALUES ( 'T12','Tablets', 450);
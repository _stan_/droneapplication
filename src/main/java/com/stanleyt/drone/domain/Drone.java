package com.stanleyt.drone.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Getter
@Setter
@ToString
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "serial_number", nullable = false, unique = true, length = 100)
    private String serialNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Model model;

    @Max(500)
    @Min(0)
    @Column(name = "weight_limit", nullable = false, scale = 1)
    private Double weightLimit;

    @Max(100)
    @Min(0)
    @Column(name = "battery_capacity", nullable = false)
    private Integer batteryCapacity;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private State state;

    @OneToOne
    @JoinColumn(name = "medication_id", referencedColumnName = "id")
    private Medication medication;
}

package com.stanleyt.drone.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Pattern;

@Entity
@Getter
@Setter
@ToString
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(nullable = false)
    @Pattern(regexp = "^[A-Za-z_-]+$", message = "Only letters, ‘-‘, ‘_’ allowed and no special characters")
    private String name;

    @Column(nullable = false)
    private Double weight;

    @Column(nullable = false, unique = true)
    @Pattern(regexp = "^[0-9A-Z_]+$", message = "Only uppercase letters, numbers, ‘-‘, ‘_’ allowed and no special characters")
    private String code;

    @Column()
    @Lob
    private byte[] image;

}

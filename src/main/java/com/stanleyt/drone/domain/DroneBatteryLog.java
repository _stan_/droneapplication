package com.stanleyt.drone.domain;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class DroneBatteryLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    @Column(name = "drone_serial_number", nullable = false,updatable = false)
    private String droneSerialNumber;

    @Max(100)
    @Min(0)
    @Column(name = "battery_capacity", nullable = false)
    private Integer batteryCapacity;

}

package com.stanleyt.drone.scheduled;

import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.domain.DroneBatteryLog;
import com.stanleyt.drone.domain.State;
import com.stanleyt.drone.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor

public class UpdateDroneBatteryCapacity {

    private final DroneRepository droneRepository;

    @Scheduled(cron = "0 55 0/1  ? * *")
    public void run(){
        droneRepository.findAll().stream().forEach(drone -> {
            DroneBatteryLog droneBatteryLog = new DroneBatteryLog();
            State state = drone.getState();
            switch (state) {
                case LOADING:
                case LOADED:
                case DELIVERED:
                    log.info("Updating drone battery capacity = {}, serialNumber = {}", drone.getBatteryCapacity()-1,
                            drone.getSerialNumber());

                    drone.setBatteryCapacity(drone.getBatteryCapacity()-1);
                    validateBatteryCapacity(drone);

                    droneRepository.save(drone);

                break;

                case DELIVERING:
                    log.info("Updating drone battery capacity = {}, serialNumber = {}", drone.getBatteryCapacity()-3,
                            drone.getSerialNumber());

                    drone.setBatteryCapacity(drone.getBatteryCapacity()-3);
                    validateBatteryCapacity(drone);

                    droneRepository.save(drone);
                    break;

                case RETURNING:
                    log.info("Updating drone battery capacity = {}, serialNumber = {}", drone.getBatteryCapacity()-2,
                            drone.getSerialNumber());

                    drone.setBatteryCapacity(drone.getBatteryCapacity()-2);
                    validateBatteryCapacity(drone);

                    droneRepository.save(drone);
                    break;

                case IDLE:
                        log.info("Updating drone battery capacity = {}, serialNumber = {}", drone.getBatteryCapacity() + 10,
                                drone.getSerialNumber());

                    drone.setBatteryCapacity(drone.getBatteryCapacity()+10);
                    validateBatteryCapacity(drone);

                    droneRepository.save(drone);
                    break;

                default:
                    log.info("User trying to update invalid state");

                    break;
            }
        });
    }

    private void validateBatteryCapacity(Drone drone) {
        if(drone.getBatteryCapacity() > 100){
            drone.setBatteryCapacity(100);
        }
        if (drone.getBatteryCapacity() < 0){
            drone.setBatteryCapacity(0);
        }
    }

}

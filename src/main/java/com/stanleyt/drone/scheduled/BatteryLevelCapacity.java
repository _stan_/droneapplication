package com.stanleyt.drone.scheduled;

import com.stanleyt.drone.domain.DroneBatteryLog;
import com.stanleyt.drone.repository.DroneBatteryLogRepository;
import com.stanleyt.drone.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;

@Component
@Slf4j
@RequiredArgsConstructor
public class BatteryLevelCapacity{

    private final DroneRepository droneRepository;
    private final DroneBatteryLogRepository droneBatteryLogRepository;


    @Scheduled(cron = "0 0 0/1  ? * *")         //every hour starting @0000hours
    //@Scheduled(fixedRate = 60000)             //testing every minute
    public void capacityScheduler(){

        droneRepository.findAll().stream().forEach(drone -> {
            log.info("Checking drone battery capacity = {}, serialNumber = {}", drone.getBatteryCapacity(),
                    drone.getSerialNumber());

            DroneBatteryLog droneBatteryLog = new DroneBatteryLog();
            droneBatteryLog.setBatteryCapacity(drone.getBatteryCapacity());
            droneBatteryLog.setDroneSerialNumber(drone.getSerialNumber());
            droneBatteryLog.setDate(LocalDateTime.now());

            droneBatteryLogRepository.save(droneBatteryLog);
        });

    }
}

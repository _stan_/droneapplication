package com.stanleyt.drone.repository;

import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.domain.State;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {
    Page<Drone> findByState(State state, Pageable pageable);
}

package com.stanleyt.drone.repository;

import com.stanleyt.drone.domain.DroneBatteryLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneBatteryLogRepository extends JpaRepository <DroneBatteryLog, Long> {

}

package com.stanleyt.drone.exception;

public class DroneLoadingException extends RuntimeException{

    public DroneLoadingException(String message) {
        super(message);
    }
}

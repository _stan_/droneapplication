package com.stanleyt.drone.service.validation;

import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.domain.Medication;

public interface DroneLoadingValidationService {
    void validateDroneBatteryCapacity (Drone drone);
    void validateDroneState(Drone drone);
    void validateDroneWeightCapacity(Drone drone, Medication medication);
}

package com.stanleyt.drone.service.validation;

import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.domain.Medication;
import com.stanleyt.drone.domain.State;
import com.stanleyt.drone.exception.DroneLoadingException;
import org.springframework.stereotype.Component;

@Component
public class DroneLoadingValidationServiceImpl implements DroneLoadingValidationService {
    @Override
    public void validateDroneBatteryCapacity(Drone drone) {
        if(drone.getBatteryCapacity() < 25){
            throw new DroneLoadingException("Drone Battery Capacity is too low");
        }
    }

    @Override
    public void validateDroneState(Drone drone) {
        if(!drone.getState().equals(State.IDLE)){
            throw new DroneLoadingException("Drone State is not eligible for Loading");
        }

    }

    @Override
    public void validateDroneWeightCapacity(Drone drone, Medication medication) {

        if(drone.getWeightLimit() < medication.getWeight()){
            throw new DroneLoadingException("Medication Weight is above Drone Capacity");
        }
    }
}

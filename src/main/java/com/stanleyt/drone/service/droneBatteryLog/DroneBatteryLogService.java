package com.stanleyt.drone.service.droneBatteryLog;

import com.stanleyt.drone.domain.DroneBatteryLog;

public interface DroneBatteryLogService {

    DroneBatteryLog createDroneBatteryLog (DroneBatteryLog droneBatteryLog);

    DroneBatteryLog loadDrone (Long droneId, Long medicationId);

    DroneBatteryLog getDrone (Long droneId);
}

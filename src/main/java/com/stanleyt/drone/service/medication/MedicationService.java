package com.stanleyt.drone.service.medication;

import com.stanleyt.drone.domain.Medication;
import org.springframework.web.multipart.MultipartFile;

public interface MedicationService {
    Medication createMedication(MedicationRequest medicationRequest);

//    Medication updateMedication();

    Medication getMedication(Long MedicationId);

    Medication updateMedication(Long medicationId, MedicationRequest medicationRequest);

    Medication uploadImage(Long medicationId, MultipartFile image);
}

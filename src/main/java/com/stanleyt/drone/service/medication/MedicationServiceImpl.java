package com.stanleyt.drone.service.medication;

import com.stanleyt.drone.domain.Medication;
import com.stanleyt.drone.exception.ImageUploadFailedException;
import com.stanleyt.drone.exception.RecordNotFoundException;
import com.stanleyt.drone.repository.MedicationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@RequiredArgsConstructor
@Slf4j
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;


    @Override
    public Medication createMedication(MedicationRequest medicationRequest) {
        Medication medication = new Medication();
        medication.setName(medicationRequest.getName());
        medication.setCode(medicationRequest.getCode());
        medication.setWeight(medicationRequest.getWeight());

        return medicationRepository.save(medication);
    }

    @Override
    public Medication getMedication(Long MedicationId) {

        return  medicationRepository.findById(MedicationId).orElseThrow(() -> new RecordNotFoundException("Medication Not Found!!"));
    }

    @Override
    public Medication updateMedication(Long medicationId, MedicationRequest medicationRequest) {
        Medication medication = getMedication(medicationId);
        medication.setWeight(medicationRequest.getWeight());
        medication.setName(medicationRequest.getName());
        medication.setCode(medicationRequest.getCode());

        return medicationRepository.save(medication);
    }

    @Override
    public Medication uploadImage(Long medicationId, MultipartFile image) {
        Medication medication = getMedication(medicationId);

        try {
            medication.setImage(image.getBytes());
        } catch (IOException e) {
            throw new ImageUploadFailedException("Failed to upload Image");
        }

        return medicationRepository.save(medication);
    }

}

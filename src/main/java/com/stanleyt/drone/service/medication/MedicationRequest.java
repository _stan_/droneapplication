package com.stanleyt.drone.service.medication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationRequest {

    @Pattern(regexp = "^[A-Za-z_-]+$", message = "Only letters, ‘-‘, ‘_’ allowed and no special characters")
    private String name;

    private Double weight;

    @Pattern(regexp = "^[0-9A-Z_]+$", message = "Only letters, numbers, ‘-‘, ‘_’ allowed and no special characters")
    private String code;

}

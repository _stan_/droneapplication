package com.stanleyt.drone.service.drone;

import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.domain.Medication;
import com.stanleyt.drone.domain.State;
import com.stanleyt.drone.exception.RecordNotFoundException;
import com.stanleyt.drone.repository.DroneRepository;
import com.stanleyt.drone.service.medication.MedicationService;
import com.stanleyt.drone.service.validation.DroneLoadingValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final MedicationService medicationService;

    private final DroneLoadingValidationService droneLoadingValidationService;

    @Override
    public Drone createDrone(DroneRequest droneRequest) {

        log.info("Creating new Drone: {}", droneRequest);
        Drone drone = new Drone();
        drone.setSerialNumber(droneRequest.getSerialNumber());
        drone.setWeightLimit(droneRequest.getWeightLimit());
        drone.setBatteryCapacity(droneRequest.getBatteryCapacity());
        drone.setModel(droneRequest.getModel());
        drone.setState(State.IDLE);

        return droneRepository.save(drone);
    }

    @Override
    public Drone updateDrone(Long droneId, DroneRequest droneRequest) {

        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> new RecordNotFoundException("Drone Not Found!!"));
        drone.setSerialNumber(droneRequest.getSerialNumber());
        drone.setState(droneRequest.getState());
        drone.setBatteryCapacity(droneRequest.getBatteryCapacity());
        drone.setModel(droneRequest.getModel());
        drone.setWeightLimit(droneRequest.getWeightLimit());

        return droneRepository.save(drone);
    }

    @Override
    public Drone loadDrone(Long droneId, Long medicationId) {

        Medication medication = medicationService.getMedication(medicationId);
        Drone drone = droneRepository.findById(droneId).orElseThrow(() -> new RecordNotFoundException("Drone Not Found!!"));

        droneLoadingValidationService.validateDroneBatteryCapacity(drone);
        droneLoadingValidationService.validateDroneState(drone);
        droneLoadingValidationService.validateDroneWeightCapacity(drone, medication);

        drone.setMedication(medication);
        drone.setState(State.LOADING);

        return droneRepository.save(drone);

    }

    @Override
    public Drone getDrone(Long droneId) {

        return  droneRepository.findById(droneId).orElseThrow(() -> new RecordNotFoundException("Drone Not Found!!"));
    }

    @Override
    public Page<Drone> getAvailableDrones(Pageable pageable) {

        return droneRepository.findByState(State.IDLE, pageable);
    }
}

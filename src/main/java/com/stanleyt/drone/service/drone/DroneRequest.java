package com.stanleyt.drone.service.drone;

import com.stanleyt.drone.domain.Model;
import com.stanleyt.drone.domain.State;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DroneRequest {

    private String serialNumber;

    private Model model;

    private Double weightLimit;

    private Integer batteryCapacity;

    private State state;

}

package com.stanleyt.drone.service.drone;

import com.stanleyt.drone.domain.Drone;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DroneService {

    Drone createDrone (DroneRequest droneRequest);

    Drone updateDrone (Long droneId, DroneRequest droneRequest);

    Drone loadDrone (Long droneId, Long medicationId);

    Drone getDrone (Long droneId);

    Page<Drone> getAvailableDrones (Pageable pageable);
}

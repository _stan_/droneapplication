package com.stanleyt.drone.api;


import com.stanleyt.drone.domain.Medication;
import com.stanleyt.drone.service.medication.MedicationRequest;
import com.stanleyt.drone.service.medication.MedicationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/medication")
public class MedicationRestController {

    private final MedicationService medicationService;

    @PostMapping
    public Medication createMedication(@RequestBody @Valid MedicationRequest medicationRequest){
        log.info("Creating Medication");
        return medicationService.createMedication(medicationRequest);
    }

    @GetMapping("/{medicationId}")
    public Medication getMedication(@PathVariable Long medicationId){
        log.info("Retrieving Medication");
        return medicationService.getMedication(medicationId);
    }

    @PutMapping("/{medicationId}")
    public Medication updateMedication(@PathVariable Long medicationId,
                                       @RequestBody @Valid MedicationRequest medicationRequest){
        log.info("Updating Medication");
        return medicationService.updateMedication(medicationId, medicationRequest);
    }

    @PutMapping("/{medicationId}/image")
    public Medication uploadImage (@PathVariable Long medicationId,
                                   @RequestParam("file") MultipartFile image){
        log.info("Uploading image");
        return medicationService.uploadImage(medicationId, image);
    }

}

package com.stanleyt.drone.api;


import com.stanleyt.drone.domain.Drone;
import com.stanleyt.drone.service.drone.DroneRequest;
import com.stanleyt.drone.service.drone.DroneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/drones")
public class DroneRestController {

    private final DroneService droneService;

    @PostMapping
    public Drone createDrone(@RequestBody DroneRequest droneRequest){
        log.info("Creating Drone");
        return droneService.createDrone(droneRequest);
    }

    @PutMapping("/{id}")
    public Drone updateDrone(@PathVariable Long id,
                             @RequestBody DroneRequest droneRequest){
        log.info("Updating Drone");
        return droneService.updateDrone(id, droneRequest);
    }

    @GetMapping("/{id}")
    public Drone getDrone(@PathVariable Long id){
        log.info("Retrieving Drone");
        return droneService.getDrone(id);
    }

    @PutMapping("/{id}/medication/{medicationId}")
    public Drone loadDrone(@PathVariable Long id,
                           @PathVariable Long medicationId){
        log.info("Loading drone");
        return droneService.loadDrone(id, medicationId);
    }

    @GetMapping("/available")
    public Page<Drone> getAvailableDrones(@PageableDefault Pageable pageable){
        log.info("Getting available drones");
        return droneService.getAvailableDrones(pageable);
    }
}
